from timetracker.tracker.models import Tbluser

from django.core.management.base import NoArgsCommand
from timetracker import settings

class Command(NoArgsCommand):
    def handle(self, **kwargs):
        '''This is what gets invoked from manage.py update_holidays.
        
        The queryset MUST be optimized because it's definitely too
        slow but as per fast request for now this form is ok.
        '''

        people = Tbluser.objects.all()

        for person in people:
            if person.holiday_26:
                person.holiday_balance += 26
            else:
                person.holiday_balance += 20

            person.save()
